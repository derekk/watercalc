var gulp  		=	require('gulp'),
	sass  		=	require('gulp-sass'), 
	watch 		=	require('gulp-watch'),
	autoprefix	=	require('gulp-autoprefixer'),
	uglify		=	require('gulp-uglify'),
	rename		=	require('gulp-rename'),
	sourcemaps	=	require('gulp-sourcemaps');

gulp.task('sass', function(){
	gulp.src('css/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({ 'outputStyle' : 'compressed' }).on('error', sass.logError))
		.pipe(autoprefix())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('httpdocs/css'));
});

gulp.task('js', function(){
	gulp.src('js/src/*.js')
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('httpdocs/js'));
});

gulp.task('html', function(){
	gulp.src('html/*.html')
		.pipe(gulp.dest('httpdocs'));
});

gulp.task('images', function(){
	gulp.src('Images/*.*')
		.pipe(gulp.dest('httpdocs/img'));
});

gulp.task('watch', function(event) {
	gulp.watch('css/*.scss', ['sass']);
	gulp.watch('js/src/*.js', ['js']);
	gulp.watch('html/*.html', ['html']);
});

gulp.task('build', ['html', 'js', 'sass', 'images']);

gulp.task('default', ['watch']);