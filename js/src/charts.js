/**
 * Created by jeffs on 8/27/2015.
 */
var cvs;
var context;
var cWidth,cHeight,cMargin,cSpace;
var cMarginSpace,cMarginHeight;
var bWidth,bMargin,totalBars,maxVal;
var bWidthMargin;
var ctr,numctr,speed;
var totLabelsOnYAxis;
var arrTTL = [];

function addToBar(mnth,val,ttlSug,ttlUser) {
	var ttlSaved=0;
	var maxHt = parseInt(localStorage["maxBar"]);
	var pX = parseInt(localStorage["preX"]);
	var pY = parseInt(localStorage["preY"]);
	cvs = document.getElementById("chtCanvas");
	context = cvs.getContext("2d");
	cMargin = 40;
	cSpace = 20;
	cHeight = (cvs.height - 2 * cMargin - cSpace) +20;
	cWidth = cvs.width - 2 * cMargin - cSpace;
	cMarginSpace = cMargin + cSpace;
	cMarginHeight = cMargin + cHeight;
	bMargin = 15;
	totalBars = 12;
	bVal = val;
	bHt = (bVal * cHeight / maxHt);
	bWidth = (cWidth / totalBars) - bMargin;
	bX = cMarginSpace + (mnth * (bWidth + bMargin)) - (bWidth / 2); // + bMargin;
	bY = cMarginHeight - bHt - 2;
	context.fillStyle = "red";
	context.beginPath();
	context.arc(bX,bY,6,0,2*Math.PI);
	context.fill();
	localStorage["preX"] = bX;
	localStorage["preY"] = bY;
	if (mnth>1) {
		context.lineWidth=3;
		context.strokeStyle="red";
		context.beginPath();
		context.moveTo(pX,pY);
		context.lineTo(bX,bY);
		context.stroke();
	}
	if (mnth==12) {
		var cnvs = document.getElementById("legendCvs");
		var cntx = cnvs.getContext("2d");
		cntx.clearRect(0,0,cnvs.width,cnvs.height);
		cntx.fillStyle="#44A8B1";
		cntx.fillRect(3,25,15,15);
		cntx.lineWidth=3;
		cntx.strokeStyle="red";
		cntx.fillStyle="red";
		cntx.beginPath();
		cntx.arc(180,35,6,0,2*Math.PI);
		cntx.fill();
		cntx.stroke();
		cntx.beginPath();
		cntx.arc(210,35,6,0,2*Math.PI);
		cntx.fill();
		cntx.stroke();
		cntx.moveTo(180,35);
		cntx.lineTo(210,35);
		cntx.stroke();
		cntx.fillStyle="black";
		cntx.font="24px Aerial";
		cntx.fillText("Total Annual Water Use (Gal):",120,17);
		cntx.fillStyle="black";
		cntx.font="18px Aerial";
		ttlSaved = ttlUser-ttlSug;
		if (ttlSaved < 0) {ttlSaved = 0};
		var sSug=addCommas(ttlSug);
		var sUser=addCommas(ttlUser);
		var sSaved=addCommas(ttlSaved);
		cntx.fillText("Estimated(" + sSug + ")",17,40);
		cntx.fillText("Actual(" + sUser + ")",220,40);
		cntx.fillText("Potential Savings: " + sSaved + " Gal", 350,40);
	}
}

function addCommas(iNum){
	return (iNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
}

function createBarChart(ttl,rW) {
	setChartText();
	cvs = rW.document.getElementById("chtCanvas");
	if (cvs && cvs.getContext) {
		context = cvs.getContext("2d");
	}
	context.clearRect(0,0,cvs.width, cvs.height);
	maxVal = 0;
	arrTTL[0]=mth[1] + "," + ttl[1];
	arrTTL[1]=mth[2] + "," + ttl[2];
	arrTTL[2]=mth[3] + "," + ttl[3];
	arrTTL[3]=mth[4] + "," + ttl[4];
	arrTTL[4]=mth[5] + "," + ttl[5];
	arrTTL[5]=mth[6] + "," + ttl[6];
	arrTTL[6]=mth[7] + "," + ttl[7];
	arrTTL[7]=mth[8] + "," + ttl[8];
	arrTTL[8]=mth[9] + "," + ttl[9];
	arrTTL[9]=mth[10] + "," + ttl[10];
	arrTTL[10]=mth[11] + "," + ttl[11];
	arrTTL[11]=mth[12] + "," + ttl[12];

	for (var i=0;i<14;i++) {
		ttl[i] = parseInt(ttl[i]);
		if ((ttl[i] * 2) > maxVal) {
			maxVal = ttl[i] * 2;
		}
		if (userInput[i] > maxVal){
			maxVal = userInput[i];
		}
	}
	localStorage["maxBar"] = maxVal;
	setChart();
	drawAxisLabelMarkers();
	drawChartWithAnimation();
}

function setChart() {
	cMargin = 40;
	cSpace = 20;
	cHeight = (cvs.height - 2 * cMargin - cSpace) +20;
	cWidth = cvs.width - 2 * cMargin - cSpace;
	cMarginSpace = cMargin + cSpace;
	cMarginHeight = cMargin + cHeight;
	bMargin = 15;
	totalBars = 12;
	bWidth = (cWidth / totalBars) - bMargin;
	totLabelsOnYAxis = 10;
	context.font = "14px Arial";
	ctr = 0;
	numctr = 100;
	speed=0;
}

function drawAxis(x,y,X,Y) {
	context.beginPath();
	context.moveTo(x,y);
	context.lineTo(X,Y);
	context.closePath();
	context.stroke();
}

function drawAxisLabelMarkers() {
	context.lineWidth = "2.0";
	// draw y axis
	drawAxis(cMarginSpace, cMarginHeight, cMarginSpace, cMargin);
	// draw x axis
	drawAxis(cMarginSpace, cMarginHeight, cMarginSpace + cWidth, cMarginHeight);
	context.lineWidth = "1.0";
	drawMarkers();
}

function drawMarkers() {
	setChartText();
	var numMarkers = parseInt(maxVal / totLabelsOnYAxis);
	context.textAlign = "right";
	context.fillStyle = "#000"; ;

	// Y Axis
	for (var i = 0; i <= totLabelsOnYAxis; i++) {
		markerVal = i * numMarkers;
		markerValHt = i * numMarkers * cHeight;
		var xMarkers = cMarginSpace - 5;
		var yMarkers = cMarginHeight - (markerValHt / maxVal);
		context.fillText(markerVal, xMarkers, yMarkers, cSpace + 10);
	}

	// X Axis
	context.textAlign = 'center';
	for (var i = 0; i < totalBars; i++) {
		arrval = arrTTL[i].split(",");
		month = arrval[0];

		markerXPos = cMarginSpace + bMargin + (i * (bWidth + bMargin)) + (bWidth / 2);
		markerYPos = cMarginHeight + 18 ;
		context.fillText(month, markerXPos, markerYPos, bWidth);
	}

	context.save();

	// Add Y Axis title
	context.translate(cMargin + 10, cHeight / 2);
	context.rotate(Math.PI * -90 / 180);
	context.fillText(tTitleTotalMth, 0, -40);

	context.restore();

	// Add X Axis Title
	context.fillText(tTitleMonth, cMarginSpace + (cWidth / 2), cMarginHeight + 40);
}

function drawChartWithAnimation() {
	// Loop through the total bars and draw
	for (var i = 0; i < totalBars; i++) {
		var arrVal = arrTTL[i].split(",");
		bVal = parseInt(arrVal[1]);
		bHt = (bVal * cHeight / maxVal); // / numctr * ctr;
		bX = cMarginSpace + (i * (bWidth + bMargin)) + bMargin;
		bY = cMarginHeight - bHt - 2;
		drawRectangle(bX, bY, bWidth, bHt, true);
	}

	// timeout runs and checks if bars have reached the desired height
	// if not, keep growing
	//if (ctr < numctr) {
	//    ctr = ctr + 1;
	//    setTimeout(arguments.callee, speed);
	//}
}

function drawRectangle(x, y, w, h, fill) {
	context.beginPath();
	context.rect(x, y, w, h);
	context.closePath();
	context.stroke();

	if (fill) {
		var gradient = context.createLinearGradient(0, 0, 0, 300);
		gradient.addColorStop(0, '#44A8B1');
		gradient.addColorStop(1, '#44A8B1');
		context.fillStyle = gradient;
		context.strokeStyle = gradient;
		context.fill();
	}
}

function makePie(rW,ttlPlant,ttlPool,ttlTurf,ttlHome,iDiv,mnth) {
	var canvas;
	var boxWidth = 20;
	var boxHeight = 15;
	var textSize = 14;
	var vSpacing = 24;
	var data = [ttlPlant,ttlPool,ttlTurf,ttlHome];
	switch (iDiv) {
		case 0:
			canvas = rW.document.getElementById("pieCanvas");
			boxWidth = 20;
			boxHeight = 15;
			textSize = 14;
			vSpacing = 24;
			data = [ttlPlant,ttlPool,ttlTurf,ttlHome*12];
			break;
		case 1:
			canvas = rW.document.getElementById("month1");
			break;
		case 2:
			canvas = rW.document.getElementById("month2");
			break;
		case 3:
			canvas = rW.document.getElementById("month3");
			break;
		case 4:
			canvas = rW.document.getElementById("month4");
			break;
		case 5:
			canvas = rW.document.getElementById("month5");
			break;
		case 6:
			canvas = rW.document.getElementById("month6");
			break;
		case 7:
			canvas = rW.document.getElementById("month7");
			break;
		case 8:
			canvas = rW.document.getElementById("month8");
			break;
		case 9:
			canvas = rW.document.getElementById("month9");
			break;
		case 10:
			canvas = rW.document.getElementById("month10");
			break;
		case 11:
			canvas = rW.document.getElementById("month11");
			break;
		case 12:
			canvas = rW.document.getElementById("month12");
			break;
	}

	var ctx = canvas.getContext("2d");
	var lastend = 0;

	var myTotal = 0;
	var myColor = ['#FFCA2D','#44A8B1','#99AA3A','#EA4D5B'];

	for(var e = 0; e < data.length; e++)
	{
		myTotal += data[e];
	}
	var dPct = [data[0]/myTotal,data[1]/myTotal,data[2]/myTotal,data[3]/myTotal];
	var hOffset = ((canvas.width / 2) - (canvas.height / 2)) - 5;
	var boxOffset = canvas.height + 20;
	var textOffset = boxOffset + boxWidth + 10;

	for (var i = 0; i < data.length; i++) {
		ctx.fillStyle = myColor[i];
		ctx.beginPath();
		ctx.moveTo(canvas.width/2-hOffset,canvas.height/2);
		ctx.arc(canvas.width/2-hOffset,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
		ctx.lineTo(canvas.width/2-hOffset,canvas.height/2);
		ctx.fill();
		lastend += Math.PI*2*(data[i]/myTotal);
	}
	ctx.fillStyle = "black";
	if (textSize==14) {
		ctx.font = "14px aerial";
	} else {
		ctx.font = "14px aerial";
	}
	ctx.fillText(pieTypes[0] + " - " + Math.round(dPct[0]*100) + "%", textOffset,textSize + vSpacing * 2);
	ctx.fillText(pieTypes[1] + " - " + Math.round(dPct[1]*100) + "%", textOffset,textSize + vSpacing * 3);
	ctx.fillText(pieTypes[2] + " - " + Math.round(dPct[2]*100) + "%",textOffset,textSize + (vSpacing) * 4);
	ctx.fillText(pieTypes[3] + " - " + Math.round(dPct[3]*100) + "%",textOffset,textSize + (vSpacing * 5));
	ctx.font = "18px aerial bold";
	ctx.fillText("Total " + mth[iDiv],textOffset, 24);
	ctx.fillRect(boxOffset - 2,vSpacing * 2,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 3,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 4,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 5,boxWidth + 4,boxHeight + 4);
	ctx.fillStyle = myColor[0];
	ctx.fillRect(boxOffset,vSpacing * 2 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[1];
	ctx.fillRect(boxOffset,vSpacing * 3 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[2];
	ctx.fillRect(boxOffset,vSpacing * 4 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[3];
	ctx.fillRect(boxOffset,vSpacing * 5 + 2,boxWidth,boxHeight);
}

function makeHHPie(rW,iToilet,iShower,iTooth,iLaundry,iDW,iClean,iDrink) {
	var canvas = rW.document.getElementById("pieHHDCanvas");
	var ctx = canvas.getContext("2d");
	var lastend = 0;
	var data = [iToilet,iShower,iTooth,iLaundry,iDW,iClean,iDrink];
	var myTotal = 0;
	var myColor = ["#434444","#EA4D5B","#99AA3A","#F7F0DC","#FFCA2D","#F89B20","#44A8B1"];

	for(var e = 0; e < data.length; e++)
	{
		myTotal += data[e];
	}

	for (var i = 0; i < data.length; i++) {
		ctx.fillStyle = myColor[i];
		ctx.beginPath();
		ctx.moveTo(canvas.width/2-115,canvas.height/2);
		ctx.arc(canvas.width/2-115,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
		ctx.lineTo(canvas.width/2-115,canvas.height/2);
		ctx.fill();
		lastend += Math.PI*2*(data[i]/myTotal);
	}
	ctx.fillStyle = "black";
	ctx.font = "18px aerial bold";
	ctx.fillText("Total Indoor Water Use",240,15);
	ctx.font = "14px aerial";
	ctx.fillText(pieHHDTypes[0],280,50);
	ctx.fillText(pieHHDTypes[1],280,68);
	ctx.fillText(pieHHDTypes[2],280,86);
	ctx.fillText(pieHHDTypes[3],280,104);
	ctx.fillText(pieHHDTypes[5],280,122);
	ctx.fillText(pieHHDTypes[4],280,140);
	ctx.fillText(pieHHDTypes[6],280,158);
	ctx.fillRect(238,40,30,15);
	ctx.fillRect(238,58,30,15);
	ctx.fillRect(238,76,30,15);
	ctx.fillRect(238,94,30,15);
	ctx.fillRect(238,112,30,15);
	ctx.fillRect(238,130,30,15);
	ctx.fillRect(238,148,30,15);
	ctx.fillStyle = myColor[0];
	ctx.fillRect(240,42,26,11);
	ctx.fillStyle = myColor[1];
	ctx.fillRect(240,60,26,11);
	ctx.fillStyle = myColor[2];
	ctx.fillRect(240,78,26,11);
	ctx.fillStyle = myColor[3];
	ctx.fillRect(240,96,26,11);
	ctx.fillStyle = myColor[4];
	ctx.fillRect(240,114,26,11);
	ctx.fillStyle = myColor[5];
	ctx.fillRect(240,132,26,11);
	ctx.fillStyle = myColor[6];
	ctx.fillRect(240,150,26,11);
}

function makeNewPie(ttlPlant,ttlPool,ttlTurf,ttlHome,iVal,mnth,ty) {
	var canvas;
	var boxWidth = 20;
	var boxHeight = 15;
	var textSize = 14;
	var vSpacing = 24;
	var data = [ttlPlant,ttlPool,ttlTurf,ttlHome];

	canvas = document.getElementById("pieCanvas");
	boxWidth = 20;
	boxHeight = 15;
	textSize = 14;
	vSpacing = 24;
	if (iVal==0) {
		ttlHome = ttlHome * 12;
	}
	data = [ttlPlant,ttlPool,ttlTurf,ttlHome];

	var ctx = canvas.getContext("2d");
	ctx.clearRect(0,0,canvas.width,canvas.height);
	var lastend = 0;

	var myTotal = 0;
	var myColor = ['#FFCA2D','#44A8B1','#99AA3A','#EA4D5B'];

	for(var e = 0; e < data.length; e++)
	{
		myTotal += data[e];
	}
	var dPct = [data[0]/myTotal,data[1]/myTotal,data[2]/myTotal,data[3]/myTotal];
	var hOffset = ((canvas.width / 2) - (canvas.height / 2)) - 5;
	var boxOffset = canvas.height + 20;
	var textOffset = boxOffset + boxWidth + 10;

	for (var i = 0; i < data.length; i++) {
		ctx.fillStyle = myColor[i];
		ctx.beginPath();
		ctx.moveTo(canvas.width/2-hOffset,canvas.height/2);
		ctx.arc(canvas.width/2-hOffset,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
		ctx.lineTo(canvas.width/2-hOffset,canvas.height/2);
		ctx.fill();
		lastend += Math.PI*2*(data[i]/myTotal);
	}
	ctx.fillStyle = "black";
	if (textSize==14) {
		ctx.font = "14px aerial";
	} else {
		ctx.font = "14px aerial";
	}
	ctx.fillText(ty[0] + " - " + Math.round(dPct[0]*100) + "%", textOffset,textSize + vSpacing * 2);
	ctx.fillText(ty[1] + " - " + Math.round(dPct[1]*100) + "%", textOffset,textSize + vSpacing * 3);
	ctx.fillText(ty[2] + " - " + Math.round(dPct[2]*100) + "%",textOffset,textSize + (vSpacing) * 4);
	ctx.fillText(ty[3] + " - " + Math.round(dPct[3]*100) + "%",textOffset,textSize + (vSpacing * 5));
	ctx.font = "18px aerial bold";
	ctx.fillText("Total " + mnth,textOffset, 24);
	ctx.fillRect(boxOffset - 2,vSpacing * 2,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 3,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 4,boxWidth + 4,boxHeight + 4);
	ctx.fillRect(boxOffset - 2,vSpacing * 5,boxWidth + 4,boxHeight + 4);
	ctx.fillStyle = myColor[0];
	ctx.fillRect(boxOffset,vSpacing * 2 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[1];
	ctx.fillRect(boxOffset,vSpacing * 3 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[2];
	ctx.fillRect(boxOffset,vSpacing * 4 + 2,boxWidth,boxHeight);
	ctx.fillStyle = myColor[3];
	ctx.fillRect(boxOffset,vSpacing * 5 + 2,boxWidth,boxHeight);
}

function printCanvas() {
	addInput();
	var mthOutTotal=0;
	var mthTotalGal=0;
	var mthTotIndoor = localStorage["mIndoor"];
	var mthPool = localStorage["mPool"].split("|").map(Number);
	var mthTurf = localStorage["mTurf"].split("|").map(Number);
	var mthPlant = localStorage["mPlant"].split("|").map(Number);
	var mthMonth = localStorage["mMonth"].split("|");
	var tPie = localStorage["pieCat"].split("|");
	var userInput = localStorage["uInput"].split("|");
	var mthIndoor = parseInt(localStorage["mIndoor"]);
	makeNewPie(mthPlant[0], mthPool[0], mthTurf[0], mthIndoor, 0, mthMonth[0],tPie);
	var dataUrl = document.getElementById("chtCanvas").toDataURL();
	var dataUrl2 = document.getElementById("legendCvs").toDataURL();
	var dataUrl3 = document.getElementById("pieCanvas").toDataURL();
	var dataUrl4 = document.getElementById("pieHHDCanvas").toDataURL();
	var windowContent = '<!DOCTYPE html>';
	windowContent += '<html>'
	windowContent += '<head><title>Water usage report</title></head>';
	windowContent += '<body>'
	windowContent += '<H1 align="center">Water Usage Report</H1>';
	windowContent += "<table id='allResults' width='800'><caption><h2>Total Annual Water Consumption</h2></caption>";
	windowContent += "<tr><th>Month</th><th>Inside</th><th>Turf</th><th>Planted Areas</th><th>Pool</th><th>Total Outdoor</th><th>Total Gallons</th><th>Actual Usage</th></tr>";
	for (var i = 1; i < 13; i++) {
	mthOutTotal = mthTurf[i] + mthPlant[i] + mthPool[i];
	mthTotalGal = mthOutTotal + parseInt(mthTotIndoor);
		windowContent += "<tr><td align='center'>" + mthMonth[i] + "</td><td align='center'>" + mthTotIndoor + "</td><td align='center'>" + mthTurf[i] + "</td><td align='center'>" + mthPlant[i] + "</td><td align='center'>" + mthPool[i] +
			"</td><td align='center'>" + mthOutTotal + "</td><td style='font-weight:bold' align='center'>" + mthTotalGal + "</td><td style='text-align: center;'><b>" + userInput[i] + "<b></td></tr>";
	}
	windowContent += '</table>';
	windowContent += '<br><br><br>';
	windowContent += '<img src="' + dataUrl2 + '">';
	windowContent += '<br><img src="' + dataUrl + '">';
	windowContent += '<br><img src="' + dataUrl3 + '">';
	windowContent += '<br><br><img src="' + dataUrl4 + '">';
	windowContent += '</body>';
	windowContent += '</html>';
	var printWin = window.open('','','width=340,height=260');
	printWin.document.open();
	printWin.document.write(windowContent);
	printWin.document.close();
	printWin.focus();
	printWin.print();
	printWin.close();
}

//barChart();