﻿/**
 * Created by jeffs on 8/10/2015.
 */
var tAddr="";
var tOpener="";
var tHome="";
var tYes="";
var tNo="No";
var mth = [];
var mthPool = [];
var mthTurfY = [];
var mthTurfN = [];
var arrPlantDens = [];
var arrPlantH2O = [];
var userInput = [];
var pieHHDTypes = [];
var pieTypes = [];
var galShower=2.6;
var dayMonth=30;
var galBath=20;
var weekMonth=4;
var galBrush=1.5;
var galLaundry=45;
var galHE=15;
var galDW=15;
var galCook=10;
var galDrink=2;
var lang="";
//var urlAddr="";
//var urlAddrSp;
var tLblOcc;
var tOcc=1;
var tOlder="";
var tNewer="";
var tTltNew="";
var tShower="";
var tShowerTip="";
var tIsBath="";
var tNumBath="";
var tBathTip="";
var tLaundry="";
var tDishes="";
var tHE="";
var tHRTitle="";
var tHRShower="";
var tHRBath="";
var tHRToilet="";
var tHRDW="";
var tHRLaundry="";
var tHRTeeth="";
var tHRClean="";
var tHRDrink="";
var tHRTotal="";
var tLandscape="";
var tHasLandscape="";
var tGrassSF="";
var tOverSeed="";
var tRockSF="";
var tDensity="";
var tDensitySparse="";
var tDensityMedium="";
var tDensityDense="";
var tPlantWater="";
var tPlantDrought="";
var tPlantDesert="";
var tPlantHighWater="";
var tHasPool="";
var tPoolSF="";
var tOpenMap="";
var tBtnSubmit="";
var tBtnMap="";
var htmlOutside="";
var htmlBath="";
var htmlBath2="";
var tTitleAllUse="";
var tTitleHome="";
var tTitlePool="";
var tTitleMonth="";
var tTitleTurf="";
var tTitlePlants="";
var tTitleOut;
var tTitleTotalMth;
var tTitleTotalYr;
var tTitleOutTtl;
var tTipLaundry="";
var tTipDish="";
var tTipOut="";
var tTipMeasure="";
var tTipCharts="";
var tTipEnd="";
var oOpt1="";
var oOpt2="";
var oOpt3="";
var tUserInput="";
var tChartText="";

function setFieldText() {
//    var params = {},
//        pairs = document.URL.split('?')
//            .pop()
//            .split('&');
//    for (var i = 0;i < pairs.length; i++) {
//        p = pairs[i].split('=');
//        params[p[0]] = p[1];
//        urlAddr = params["addr"];
//        lang = params["lang"];
//}
	//var oOpt1 = "";
	//var oOpt2 = "";
	//var oOpt3 = "";
	for (var i=1;i<16;i++)
	{
		oOpt1 += "<option value=" + i + ">" + i + "</option>";
	}
	for (var i=5;i<31;i++)
	{
		oOpt2 += "<option value=" + i + ">" + i + "</option>";
	}
	for (var i=5;i<10;i++)
	{
		oOpt3 += "<option value=" + i + ">" + i + "</option>";
	}

	mthPool = [0,2,2,3,5,6,7,6,6,5,4,2,1];
	mthTurfN = [0,.2,.4,1,2.5,4.3,4.7,4.5,3.9,2.3,1.1,.4,.1];
	mthTurfY = [0,1.1,1.5,2.4,3.5,4.3,4.7,4.5,3.9,3.3,3.3,1.6,1];
	arrPlantDens = [0,.15,.4,.7];
	arrPlantH2O = [0,.15,.3,.7];

	setChartText();
	if (lang=='es')
	{
		tYes="S&iacute;";
		tAddr = "Direcci&oacute;n: ";
		tOpener="El agua es preciosa aquí en el desierto . Su factura de servicios públicos le indica la cantidad de agua utiliza su hogar , pero está usando la cantidad correcta? La comparación de las facturas de servicios con nuestros vecinos puede ser inexacta . Hay muchas cosas que tendrán un impacto en el uso del agua que es único para cada hogar . Esta herramienta le permite introducir información sobre los hábitos de uso del agua de su hogar . Con base en la información que proporciona , la calculadora de agua estimará la cantidad de agua que necesita.";
		tHome="Las casas que fueron construidas despu&eacute; de 1994 tienen inodoros que usan menos agua que las mas viejas." +
				"Este tiene un gran efecto en la cantidad del agua usado en el hogar. Si su casa fue construido antes de 1994 " +
				"le va a preguntar si los inodoros han side reemplezado.";
		tOcc = "Cada persona usa su propia cantidad del agua cada d&iacute;a. Escoja cu&aacute;ntas personas vive(n) en el hogar: ";
		tLblOcc = "Eloja el n&uacute;mero de habitantes: ";
		tOlder = 'La casa fue construido antes de 1994';
		tNewer = 'La casa fue construido entre 1994 y ahora';
		tTltNew = "Los inodores han sido cambiados desde 1994";
		tShower = "&iquest;Cu&aacute;l es el promedio tiempo que los miembros de la familia pasan duchandose? (minutos)";
		tShowerTip = "* Consejo: El promedio tiempo que una persona pasa por la ducha es de 8 minutos";
		tIsBath = "&iquest;Hay alguien en el hogar que tome ba&ntilde;os?";
		tNumBath = "&iquest;Cu&aacute;ntas veces algunos miembros de la familia ba&ntilde;arse por semana?";
		tBathTip="* Consejo: Casi todas las ba&ntilde;eras mantienen 60 gallones del agua cuando llenas. Si la ba&ntilde;era s&oacute;lo est&aacute; llena de un quarto, "
		+ "s&oacute;lo use 15 gallones del agua";
		tLaundry = "&iquest;Cu&aacute;ntas cargas de ropa se lava en la lavadora cada semana?";
		tHE = "Tengo una lavadora de alta eficiencia";
		tDishes = "&iquest;Cu&aacute;ntas cargas de platos se lava en el lavavajillas cada semana?";
		tHRTitle = "Cantidad del agua usado en hogar";
		tHRToilet = "El agua usado por los inodores: ";
		tHRShower = "El agua usado por las duchas: ";
		tHRBath = "El agua usado por los ba&ntilde;os: ";
		tHRDW = "El agua usado por el lavavajillas: ";
		tHRLaundry = "El agua usado por la lavadora: ";
		tHRDrink = "El agua usado por tomar: ";
		tHRClean = "El agua usado por limpiar y cocinar: ";
		tHRTeeth = "El agua usado por cepillar los dientes: ";
		tHRTotal = "Cantidad del agua usado mensual en el hogar: ";
		tLandscape="";
		tHasLandscape="Este lugar no tiene un jard&iacute;n, o la irrigaci&oacute;n est&aacute; de otro fuente";
		tGrassSF="&iquest;Cu&aacute;ntas pies cuadrados de c&eacute;sped tiene el jard&iacute;n?";
		tOverSeed="El c&eacute;sped suele est&aacute; sembrado de nuevo por invierno";
		tRockSF="&iquest;Cu&aacute;ntas pies cuadrados de roca tiene el jard&iacute;n?";
		tDensity="&iquest;C&oacute;mo describir&iacute;a la densidad de las plantas en el jard&iacute;n?";
		tDensitySparse="Escaso";
		tDensityMedium="Medio";
		tDensityDense="Denso";
		tPlantWater="&iquest;C&oacute;mo describir&iacute;a la mayor&iacute;a de las plantas y &aacute;rboles en el jard&iacute;n?";
		tPlantDrought="Mucho tolerenc&iacute;a de sequ&iacute;a";
		tPlantDesert="Acustumbrado del desierto";
		tPlantHighWater="Alto nivel de uso de agua";
		tHasPool="Esta propiadad cuenta con una piscina";
		tPoolSF="&iquest;Cu&aacute;ntas pies cuadrado es la superficie de la piscina?";
		tOpenMap = "Abrir una p&aacute;gina web que tiene un mapa para que mida las &aacute;reas de c&eacute;sped, rocas, y la superficie de la piscina";
		tBtnSubmit = "Enviar los datos";
		tBtnMap = "Mostrar el mapa";
		tTitleAllUse = "Toda el consumo del agua anular";
		tTitleHome = "Adentro";
		tTitlePool = "Piscina";
		tTitleMonth = "Mes";
		tTitleOut = "Afuera";
		tTitlePlants = "&Aacute;reas plantadas";
		tTitleTurf = "C&eacute;sped";
		tTitleOutTtl = "Todo afuera";
		tChartText = "Ahora usted tiene una estimación de la cantidad de agua que su hogar necesita cada mes y durante todo el año . Introduzca los datos de uso del agua de su cuenta en las áreas previstas en la tabla de resultados de uso de agua. Ahora usted puede comparar la cantidad de agua que necesita en su hogar a la cantidad que realmente utiliza.";
		pieHHDTypes = ["Inodoros","Duchas","Limpiar los dientes","Limpiar Ropa","Cocinar y Limpiar","Lavar Vajilla","Agua para tomar"];
		pieTypes = ["Plantas","Piscina","Hierba","Adentro"];
	}
	else {
		tYes = "Yes";
		tAddr = "Address: ";
		tOpener = "Water is precious here in the desert. Your utility bill tells you how much water your household uses, but are you using the right amount? Comparing utility bills with our neighbors may be inaccurate. Many things will impact the water use that is unique to each household. This tool allows you to enter information about your household’s water use habits. Based on the information you provide, the water calculator will estimate how much water you need.";
		tHome = "Homes built after 1994 have toilets that use less water than the older ones. " +
			"This has a large impact on how much water is used inside the house. If your house was built before 1994 you " +
			"will be asked if the toilets have been replaced with newer more efficient ones.";
		tOcc = 'Each person uses a certain amount of water each day. Select the number of people that live in the household here: ';
		tLblOcc = "Select number of occupants: ";
		tOlder = "House was built before 1994";
		tNewer = "House was built from 1994 - present";
		tTltNew = "The toilets have been replaced since 1994";
		tShower = "What is the average time family members spend in the shower? (minutes)";
		tShowerTip = "* Tip: The average time a person spends in the shower is 8 minutes";
		tIsBath = "Does anyone take baths in the home?";
		tNumBath = "How many baths are taken by family members weekly?";
		tBathTip = "* Tip: Most bathtubs will hold 60 gallons of water when completely full. If you only fill the tub 1/4 full then "
			+ "you only use 15 gallons per bath";
		tLaundry = "How many loads of laundry are done each week?";
		tHE = "I have a high efficiency washing machine";
		tDishes = "How many dishwasher loads are done each week?";
		tHRTitle = "Indoor water use";
		tHRToilet = "Water used flushing toilets: ";
		tHRShower = "Water used showering: ";
		tHRBath = "Water used for baths: ";
		tHRDW = "Water used by dishwasher: ";
		tHRLaundry = "Water used by washing machine: ";
		tHRDrink = "Water used for drinking: ";
		tHRClean = "Water used for cleaning and cooking: ";
		tHRTeeth = "Water used brushing teeth: ";
		tHRTotal = "Total monthly indoor water use: ";
		tLandscape = "";
		tHasLandscape = "This property does not have a yard, or the irrigation comes from another source";
		tGrassSF = "How many square feet of grass are on the property?";
		tOverSeed = "The lawn is usually overseeded for winter";
		tRockSF = "How many square feet of rock are on the property?";
		tDensity = "How would you describe the density of the planted area?";
		tDensitySparse = "Sparse";
		tDensityMedium = "Medium";
		tDensityDense = "Dense";
		tPlantWater = "How would you describe the majority of plants and trees in the yard?";
		tPlantDrought = "Very drought tolerant";
		tPlantDesert = "Desert adapted";
		tPlantHighWater = "High water use";
		tHasPool = "This property has a pool";
		tPoolSF = "What is the square footage of the pool surface?";
		tOpenMap = "Open a new wepage with a map in order to measure areas of grass, rock, and pool surface";
		tBtnSubmit = "Submit Info";
		tBtnMap = "Show the map";
		tTitleAllUse = "Total annual water consumption";
		tTitleHome = "Inside";
		tTitlePool = "Pool";
		tTitleMonth = "Month";
		tTitleOut = "Outdoor";
		tTitlePlants = "Planted Areas";
		tTitleTurf = "Turf";
		tTitleOutTtl = "Total outdoor";
		tTipLaundry = "* Tip: Older top load washing machines use an average of 40-45 gallons per load. The new high efficiency " +
			"machines only use around 15 gallons per load. This saves water but can also save energy if warm or hot water " +
			"is used for the wash cycle.";
		tTipDish = "* Tip: Older models of dishwashers use an average of 15 gallons per load. The new high efficiency dishwashers use 4 to 7 gallons per cycle.";
		tTipOut = "The average outdoor water use accounts for 50 to 70% of a households annual water consumption. The size and types of landscapes have a large " +
			"impact on how much water this will actually be. Swimming pools add up to more water use through evaporation.";
		tTipMeasure = "Are you a Gilbert resident? " + 
			"Click on the globe to determine your yard's square footage. ";
		tUserInput = "Input Actual Usage";
		tChartText = "Now you have an estimate of how much water your household needs each month and for the entire year. Enter the water usage data from your account into the provided areas in the water usage results table. Now you can compare how much water you need at your household to how much you actually use.";
		pieHHDTypes = ["Toilets","Bathing","Brushing Teeth","Laundry","Cooking/Cleaning","Dishwashing","Drinking"];
		pieTypes = ["Plants","Pool","Turf","Indoor"];
	}
	//if (urlAddr === undefined || urlAddr === null){
	//    urlAddr = "";
	//}
	//urlAddrSp = urlAddr;
	//ReplaceURLSpaces();
	document.getElementById("IntroPara").innerHTML = tOpener;
	//if (urlAddr.length > 0) {
	//    document.getElementById("lblAddress").innerHTML = tAddr + urlAddrSp;
	//} else {
	//    document.getElementById("lblAddress").innerHTML = "";
	//}
	document.getElementById("lblOcc").insertAdjacentHTML('afterbegin', tOcc);
	document.getElementById("nOcc").insertAdjacentHTML('afterbegin', tLblOcc);
	document.getElementById("HomeDesc").insertAdjacentHTML('afterbegin', tHome);
	document.getElementById("lblOld").insertAdjacentHTML('beforeend', tOlder);
	document.getElementById("lblNew").insertAdjacentHTML('beforeend', tNewer);
	document.getElementById("lblShower").insertAdjacentHTML('afterbegin', tShower);
	document.getElementById("lblLaundry").insertAdjacentHTML('afterbegin', tLaundry);
	document.getElementById("lblHE").insertBefore(document.createTextNode(tHE), document.getElementById("chkHE").nextSibling);
	document.getElementById("lblDishes").insertAdjacentHTML('afterbegin', tDishes);

	//document.getElementById("numPersons").innerHTML = oOpt1;
	for (i=1;i<13;i++) {
		var opt = document.createElement("OPTION");
		opt.text = i.toString();
		opt.value = i;
		document.getElementById("numPersons").options.add(opt);
	}
	//document.getElementById("minShower").innerHTML = oOpt2;
	for (i=4;i<31;i++) {
		var opt = document.createElement("OPTION");
		opt.text = i.toString();
		opt.value = i;
		document.getElementById("minShower").options.add(opt);
		document.getElementById("minShower").value = "8";
	}
	//document.getElementById("numLaundry").innerHTML = oOpt1;
	for (i=0;i<13;i++) {
		var opt = document.createElement("OPTION");
		opt.text = i.toString();
		opt.value = i;
		document.getElementById("numLaundry").options.add(opt);
	}
	//document.getElementById("numDishes").innerHTML = oOpt1;
	for (i=0;i<13;i++) {
		var opt = document.createElement("OPTION");
		opt.text = i.toString();
		opt.value = i;
		document.getElementById("numDishes").options.add(opt);
	}
	document.getElementById("oData").insertAdjacentHTML('beforeend', tHasLandscape);
	document.getElementById("tipShower").innerHTML = tShowerTip;
	document.getElementById("isBathTaken").insertAdjacentHTML('afterbegin', tIsBath);
	document.getElementById("lblBathYes").insertAdjacentHTML('beforeend', tYes);
	document.getElementById("tipLaundry").innerHTML = tTipLaundry;
	document.getElementById("tipDish").innerHTML = tTipDish;
	document.getElementById("submitInfo").value = tBtnSubmit;
	setBathHTML();
	setOutsideInfoHTML();

	document.getElementById("outsideInfo").innerHTML = htmlOutside;
	document.getElementById("seeMapTD").insertAdjacentHTML('afterbegin', "<input type='image' src='img/globe.png' width='50' height='50' name='seeMap' id='seeMap' onclick='openMap()'>");
	document.getElementById("seeMap").value = tBtnMap;
	document.getElementById("submitInfo").value = tBtnSubmit;
}

function setChartText() {
	if (lang=='es')
	{
		mth = ["Todo","Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"];
		tTitleTotalMth = "Total (mensual)";
		tTitleTotalYr = "Total (anular)";
	} else {
		mth = ["Year", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		tTitleTotalMth = "Total Gallons (monthly)";
		tTitleTotalYr = "Total (annual)";
	}
}

function setOutsideInfoHTML() {

	htmlOutside += "<p id='descOut'>" + tTipOut + "</p>";
	htmlOutside += "<div id='seeMapTD'><p id='tipMeasure'>" + tTipMeasure + "</p></div>";
	htmlOutside += "<fieldset><label>" + tGrassSF + "<input type='text' id='txtGrass' size='4'></label>";
	htmlOutside += "<label><input type='checkbox' name='chkOver' id='chkOver'>" + tOverSeed + "</label>";
	htmlOutside += "<label>" + tRockSF + "<input type='text' id='txtRock' size='4'></label>";
	htmlOutside += "<label>" + tDensity + "<select name='selDensity' id='selDensity'>";
	htmlOutside += "<option value=1>" + tDensitySparse + "</option>";
	htmlOutside += "<option value=2>" + tDensityMedium + "</option>";
	htmlOutside += "<option value=3>" + tDensityDense + "</option>";
	htmlOutside += "</select></label>";
	htmlOutside += "<label>" + tPlantWater + "<select name='selH2O' id='selH2O'>";
	htmlOutside += "<option value=1>" + tPlantDrought + "</option>";
	htmlOutside += "<option value=2>" + tPlantDesert + "</option>";
	htmlOutside += "<option value=3>" + tPlantHighWater + "</option>";
	htmlOutside += "</select></label>";
	htmlOutside += "<label><input type='checkbox' name='chkPool' id='chkPool' onclick='CheckPool(this)'>" + tHasPool + "</label>";
	htmlOutside += "<div id='PoolSqFt'></div></fieldset>";

	//htmlOutside += "</form>";
}

function CheckToilets(val) {
   // window.alert(val);
	if (val == "Older") {
		var newTlt = document.createElement("input");
		newTlt.type = "checkbox";
		newTlt.name = "newToilet";
		newTlt.value = "value";
		newTlt.id = "newTlt";
		var lbl = document.createElement("label");
		//lbl.htmlFor = "newTlt";
		lbl.id = "tltLabel";
		lbl.appendChild(document.createTextNode(tTltNew));
		document.getElementById("toilets").insertBefore(lbl, document.getElementById("lblOld").nextSibling);
		lbl.insertBefore(newTlt, lbl.firstChild);
	} else {
		var prt = document.getElementById("toilets");
		var lbl = document.getElementById("tltLabel");
		prt.removeChild(lbl);
	}
}

function setBathHTML() {
	htmlBath += "<label id='lblBath' title=''><select name='numBath' id='numBath'></select>";
	htmlBath += "<a class='tooltips' href='#/'><img src='img/QuickTipButton.png'><span><p id ='tipBath'></p></span></a></label>";
}

function HasBath(val) {
	var bHTML="";
	if (val == "bthYes") {
	   document.getElementById("bathDiv").innerHTML = htmlBath;
	   document.getElementById("tipBath").innerHTML = tBathTip;
	   document.getElementById("lblBath").title="Baths taken per week";
		for (i=1;i<13;i++) {
			var opt = document.createElement("OPTION");
			opt.text = i.toString();
			opt.value = i;
			document.getElementById("numBath").options.add(opt);
		}
		document.getElementById("lblBath").insertAdjacentHTML('afterbegin', tNumBath);
	} else {
		document.getElementById("bathDiv").innerHTML = "";
		//document.getElementById("BathTR2").innerHTML = "";
   }
}

function CheckOutside(cb) {
	// window.alert(val);
	if (cb.checked) {
		//window.alert("checked");
		document.getElementById("outsideInfo").innerHTML = "";
	} else
	{
		//window.alert(htmlOutside);
		document.getElementById("outsideInfo").innerHTML = htmlOutside;
		document.getElementById("seeMapTD").insertAdjacentHTML('afterbegin', "<input type='image' src='img/globe.png' width='100' height='100' name='seeMap' id='seeMap' onclick='openMap()'>");
		document.getElementById("seeMap").value = tBtnMap;
	}
}

function CheckPool(cb) {
	// window.alert(val);
	if (cb.checked) {
		//window.alert("checked");
		document.getElementById("PoolSqFt").innerHTML = tPoolSF + "<input type='text' id='poolSF' size='4'>";
	} else
	{
		//window.alert(htmlOutside);
		document.getElementById("PoolSqFt").innerHTML = "";
	}
}

function getTltSize() {
	var iSize=1.6;
	if (document.getElementById("ageOld").checked == true) {
		if (document.getElementById("newTlt").checked == false) {
			iSize = 3.5;
		}
	}
	return iSize;
}

function getLaundryGallons(){
	var iGal=galLaundry;
	if (document.getElementById("chkHE").checked == true)
	{
		iGal = galHE;
	}
	return iGal;
}

function openMap(){
	window.open("http://www.gilbertmapping.com/Measure/Measure.html");
}

function calcOuterValues(ttl,rWin,hhToilet,hhShower,hhTooth,hhLaundry,hhDW,hhClean,hhDrink) {
	var turfSF = 0;
	var rockSF = 0;
	var pSF = 0;
	var tSF = 0;
	var pDens = 0;
	var pH2O = 0;
	var plSF = 0;
	var tOver = false;
	var mthTurfTtl = [];
	var mthPlantTtl = [];
	var mthPoolTtl = [];
	var mthOutTtl = [];
	var mthAllTtl = [];
	var mthTurfGTtl = 0;
	var mthPlantGTtl = 0;
	var mthOutGTtl = 0;
	var mthAllGTtl = 0;
	var mthPoolGTtl = 0;

	pDens = arrPlantDens[document.getElementById("selDensity").value];
	pH2O = arrPlantH2O[document.getElementById("selH2O").value];
	//window.alert(document.getElementById("selDensity").value);
	//window.alert(document.getElementById("selH2O").value);

	plSF = document.getElementById("txtRock").value;
	if (isNaN(document.getElementById("txtRock").value)) {
		plSF = 0;
	}
	if (document.getElementById("chkPool").checked==true) {
		pSF = document.getElementById("poolSF").value;
		if (isNaN(document.getElementById("poolSF").value)) {
			pSF = 0;
		}
	} else {
		pSF = 0;
	}
	tSF = document.getElementById("txtGrass").value;
	if (isNaN(document.getElementById("txtGrass").value)) {
		tSF = 0;
	}
	if (document.getElementById("chkOver").checked==true) {
		tOver = true;
	}
	var resultHTML = "";
	resultHTML += "<form method='post' action='javascript:addInput()'>";
	resultHTML += "<h2>" + tTitleAllUse + "</h2><table id='allResults'>";
	resultHTML += "<thead><tr><th>" + tTitleMonth + "</th><th>" + tTitleHome + "</th><th>" + tTitleTurf + "</th><th>" + tTitlePlants +
		"</th><th>" + tTitlePool + "</th><th>" + tTitleOutTtl + "</th><th>" + tTitleTotalMth + "</th><th>" + tUserInput + "</th></tr></thead><tbody>";
	for (var i = 1; i < 13; i++) {
		mthPoolTtl[i] = Math.round(pSF * mthPool[i]);
		mthPoolGTtl += mthPoolTtl[i];
		if (tOver==true){
			mthTurfTtl[i] = Math.round(tSF * mthTurfY[i]);
		} else {
			mthTurfTtl[i] = Math.round(tSF * mthTurfN[i]);
		}
		mthTurfGTtl += mthTurfTtl[i];
		mthPlantTtl[i] = Math.round((plSF * mthPool[i] * pDens * pH2O));

		mthPlantGTtl += mthPlantTtl[i];
		mthOutTtl[i] = mthPoolTtl[i] + mthTurfTtl[i] + mthPlantTtl[i];
		mthAllTtl[i] = mthPoolTtl[i] + ttl + mthTurfTtl[i] + mthPlantTtl[i];
		mthOutGTtl += mthOutTtl[i];
		mthAllGTtl += mthAllTtl[i];
		resultHTML += '<tr><td data-th="' + tTitleMonth + '">' + mth[i] + '</td><td data-th="' + tTitleHome + '">' + ttl + '</td><td data-th="' + tTitleTurf + '">' + mthTurfTtl[i] + '</td><td data-th="' + tTitlePlants + '">' + mthPlantTtl[i] + '</td><td data-th="' + tTitlePool + '">' + mthPoolTtl[i] +
			'</td><td data-th="' + tTitleOutTtl + '">' + mthOutTtl[i] + '</td><td data-th="' + tTitleTotalMth + '" style="font-weight:bold">' + mthAllTtl[i] + '</td><td data-th="' + tUserInput + '"><input type="text" id="uInput' + i + '" size="5"></tr>';
	}
	localStorage["mthAllTtl"] = mthAllTtl;
	localStorage["mPool"] = mthPoolGTtl + "|" + mthPoolTtl[1] + "|" + mthPoolTtl[2] + "|" + mthPoolTtl[3] + "|" + mthPoolTtl[4] + "|" + mthPoolTtl[5] + "|"
		+ mthPoolTtl[6]  + "|" + mthPoolTtl[7] + "|" + mthPoolTtl[8] + "|" + mthPoolTtl[9] + "|" + mthPoolTtl[10] + "|" + mthPoolTtl[11] + "|" + mthPoolTtl[12]
	localStorage["mTurf"] = mthTurfGTtl + "|" + mthTurfTtl[1] + "|" + mthTurfTtl[2] + "|" + mthTurfTtl[3] + "|" + mthTurfTtl[4] + "|" + mthTurfTtl[5] + "|"
		+ mthTurfTtl[6] + "|" + mthTurfTtl[7] + "|" + mthTurfTtl[8] + "|" + mthTurfTtl[9] + "|" + mthTurfTtl[10] + "|" + mthTurfTtl[11] + "|" + mthTurfTtl[12];
	localStorage["mPlant"] = mthPlantGTtl + "|" + mthPlantTtl[1] +"|" + mthPlantTtl[2] +"|" + mthPlantTtl[3] +"|" + mthPlantTtl[4] +"|" + mthPlantTtl[5] +"|"
		+ mthPlantTtl[6] +"|" + mthPlantTtl[7] +"|" + mthPlantTtl[8] +"|" + mthPlantTtl[9] +"|" + mthPlantTtl[10] +"|" + mthPlantTtl[11] +"|" + mthPlantTtl[12] +"|";
	localStorage["mIndoor"] = ttl;
	localStorage["mMonth"] = mth[0] + "|" + mth[1] + "|" + mth[2] + "|" + mth[3] + "|" + mth[4] + "|" + mth[5] + "|" + mth[6] + "|" + mth[7] + "|" + mth[8] + "|"
		+ mth[9] + "|" + mth[10] + "|" + mth[11] + "|" + mth[12];
	localStorage["pieCat"] = pieTypes[0] + "|" + pieTypes[1] + "|" + pieTypes[2] + "|" + pieTypes[3];
	localStorage["totalSuggested"] = mthAllGTtl;
	resultHTML += "</tbody><tfoot><tr><td data-th='" + tTitleMonth + "'>" + tTitleTotalYr + "</td><td data-th='" + tTitleHome + "'>" + (ttl * 12) + "</td><td data-th='" + tTitleTurf + "'>" + mthTurfGTtl + "</td><td data-th='" + tTitlePlants + "'>" +
		mthPlantGTtl + "</td><td data-th='" + tTitlePool + "'>" + mthPoolGTtl + "</td><td data-th='" + tTitleOutTtl + "'>" + mthOutGTtl + "</td><td data-th='" + tTitleTotalMth + "'>" + mthAllGTtl + "</td><td data-th='" + tUserInput + "' id='uInfo'></td></td></tr>";
	resultHTML += "<tr><td colspan=7></td><td><input type='submit' id='uInfoSubmit' value='" + tBtnSubmit + "'></td></tr></tfoot>";
	resultHTML += "</table>";
	resultHTML += "</form>";
	rWin.document.getElementById("OResults").innerHTML = resultHTML;

	//document.getElementById("allResults").innerHTML = resultHTML;
	createBarChart(mthAllTtl,rWin);
	makePie(rWin,mthPlantGTtl,mthPoolGTtl,mthTurfGTtl,ttl,0);
	makeHHPie(rWin,hhToilet,hhShower,hhTooth,hhLaundry,hhDW,hhClean,hhDrink);
	rWin.document.getElementById("mth0").innerHTML = mth[0];
	rWin.document.getElementById("mth1").innerHTML = mth[1];
	rWin.document.getElementById("mth2").innerHTML = mth[2];
	rWin.document.getElementById("mth3").innerHTML = mth[3];
	rWin.document.getElementById("mth4").innerHTML = mth[4];
	rWin.document.getElementById("mth5").innerHTML = mth[5];
	rWin.document.getElementById("mth6").innerHTML = mth[6];
	rWin.document.getElementById("mth7").innerHTML = mth[7];
	rWin.document.getElementById("mth8").innerHTML = mth[8];
	rWin.document.getElementById("mth9").innerHTML = mth[9];
	rWin.document.getElementById("mth10").innerHTML = mth[10];
	rWin.document.getElementById("mth11").innerHTML = mth[11];
	rWin.document.getElementById("mth12").innerHTML = mth[12];
	//for (var x=1;x<13;x++)
	//{
	//    makePie(rWin, mthPlantTtl[x], mthPoolTtl[x], mthTurfTtl[x], ttl, x, mth[x]);
	//}

}

function addInput() {
	if (isNaN(document.getElementById("uInput1").value))
	{
		userInput[1] = 0;
	}
	else {
		userInput[1] = document.getElementById("uInput1").value;
	}
	if (isNaN(document.getElementById("uInput2").value))
	{
		userInput[2] = 0;
	}
	else {
		userInput[2] = document.getElementById("uInput2").value;
	}
	if (isNaN(document.getElementById("uInput3").value))
	{
		userInput[3] = 0;
	}
	else {
		userInput[3] = document.getElementById("uInput3").value;
	}
	if (isNaN(document.getElementById("uInput4").value))
	{
		userInput[4] = 0;
	}
	else {
		userInput[4] = document.getElementById("uInput4").value;
	}
	if (isNaN(document.getElementById("uInput5").value))
	{
		userInput[5] = 0;
	}
	else {
		userInput[5] = document.getElementById("uInput5").value;
	}
	if (isNaN(document.getElementById("uInput6").value))
	{
		userInput[6] = 0;
	}
	else {
		userInput[6] = document.getElementById("uInput6").value;
	}
	if (isNaN(document.getElementById("uInput7").value))
	{
		userInput[7] = 0;
	}
	else {
		userInput[7] = document.getElementById("uInput7").value;
	}
	if (isNaN(document.getElementById("uInput8").value))
	{
		userInput[8] = 0;
	}
	else {
		userInput[8] = document.getElementById("uInput8").value;
	}
	if (isNaN(document.getElementById("uInput9").value))
	{
		userInput[9] = 0;
	}
	else {
		userInput[9] = document.getElementById("uInput9").value;
	}
	if (isNaN(document.getElementById("uInput10").value))
	{
		userInput[10] = 0;
	}
	else {
		userInput[10] = document.getElementById("uInput10").value;
	}
	if (isNaN(document.getElementById("uInput11").value))
	{
		userInput[11] = 0;
	}
	else {
		userInput[11] = document.getElementById("uInput11").value;
	}
	if (isNaN(document.getElementById("uInput12").value))
	{
		userInput[12] = 0;
	}
	else {
		userInput[12] = document.getElementById("uInput12").value;
	}
	var iTTLUser = 0;
	for(i=1;i<13;i++) {
		if(userInput[i]===undefined){
			userInput[i]=0;
		}
		if(isNaN(userInput[i]) || userInput[i]=="") {
			userInput[i]=0;
		}
		iTTLUser+= parseInt(userInput[i]);
	}
	//alert(userInput[1]);
	//alert(userInput[2]);
	//alert(userInput[3]);
	localStorage["uInput"] = iTTLUser;
	var mthTotal = localStorage["mthAllTtl"].split(",");
	createBarChart(mthTotal, window);
	for(i=1;i<13;i++) {
		localStorage["uInput"] = localStorage["uInput"] + '|' + userInput[i];
		addToBar(i,userInput[i],localStorage["totalSuggested"],iTTLUser);
	}
	// document.getElementById("uInput1").disabled = true;
	// document.getElementById("uInput2").disabled = true;
	// document.getElementById("uInput3").disabled = true;
	// document.getElementById("uInput4").disabled = true;
	// document.getElementById("uInput5").disabled = true;
	// document.getElementById("uInput6").disabled = true;
	// document.getElementById("uInput7").disabled = true;
	// document.getElementById("uInput8").disabled = true;
	// document.getElementById("uInput9").disabled = true;
	// document.getElementById("uInput10").disabled = true;
	// document.getElementById("uInput11").disabled = true;
	// document.getElementById("uInput12").disabled = true;
	document.getElementById("uInfo").innerHTML = iTTLUser.toString();
	document.getElementById("display").scrollIntoView(true);
}

function doMO(val) {
	//alert(val);
	var mthPool = localStorage["mPool"].split("|").map(Number);
	var mthTurf = localStorage["mTurf"].split("|").map(Number);
	var mthPlant = localStorage["mPlant"].split("|").map(Number);
	var mthMonth = localStorage["mMonth"].split("|");
	var tPie = localStorage["pieCat"].split("|");
	//alert(localStorage["mMonth"]);
	var mthIndoor = parseInt(localStorage["mIndoor"]);
	makeNewPie(mthPlant[val], mthPool[val], mthTurf[val], mthIndoor, val, mthMonth[val],tPie);
	//alert(localStorage["mPool"]);
	//alert(localStorage["mTurf"]);
	//alert(localStorage["mPlant"]);
	//alert(localStorage["mIndoor"]);
}

function calcValues() {
	var hhdPersons = document.getElementById("numPersons").value;
	var galToilet = getTltSize();
	var ttlFlush = Math.round(galToilet * 5 * hhdPersons * dayMonth);
	var ttlShower = Math.round(document.getElementById("minShower").value * hhdPersons * galShower * dayMonth);
	if (document.getElementById("numBath") === null || document.getElementById("numBath") === undefined) {
		ttlBath = 0;
	} else {
		var ttlBath = Math.round(document.getElementById("numBath").value * galBath * weekMonth);
	}
	var ttlDW = Math.round(document.getElementById("numDishes").value * galDW * weekMonth);
	var ttlLaundry = Math.round(getLaundryGallons() * document.getElementById("numLaundry").value * weekMonth);
	var ttlTooth = Math.round(hhdPersons * galBrush * dayMonth);
	var ttlClean = Math.round(hhdPersons * galCook * dayMonth);
	var ttlDrink = Math.round(hhdPersons * galDrink * dayMonth);
	var ttlTodalHhd = ttlFlush + ttlShower + ttlBath + ttlDW + ttlLaundry + ttlTooth + ttlClean + ttlDrink;
	var resultHTML = "";
	resultHTML = "<table id='hhdResults'>"
	resultHTML += "<h2>" + tHRTitle + "</h2>";
	resultHTML += "<tr><td>" + tHRToilet + "</td><td>" + ttlFlush + "</tr></td>";
	resultHTML += "<tr><td>" + tHRShower + "</td><td>" + ttlShower + "</tr></td>";
	resultHTML += "<tr><td>" + tHRBath + "</td><td>" + ttlBath + "</tr></td>";
	resultHTML += "<tr><td>" + tHRDW + "</td><td>" + ttlDW + "</tr></td>";
	resultHTML += "<tr><td>" + tHRLaundry + "</td><td>" + ttlLaundry + "</tr></td>";
	resultHTML += "<tr><td>" + tHRTeeth + "</td><td>" + ttlTooth + "</tr></td>";
	resultHTML += "<tr><td>" + tHRClean + "</td><td>" + ttlClean + "</tr></td>";
	resultHTML += "<tr><td>" + tHRDrink + "</td><td>" + ttlDrink + "</tr></td>";
	resultHTML += "<tr style='font-weight:bold'><td>" + tHRTotal + "</td><td>" + ttlTodalHhd;
	resultHTML += "</table>";

	//document.getElementById("Results").innerHTML = resultHTML;
	var resWin = window.open("WRResults.html");
	updateResults();

	function updateResults(){
		var resultsDiv = resWin.document.getElementById("IResults"),
			introParagraph = resWin.document.getElementById("intPar");
		if ((resultsDiv) && (introParagraph)) {
			resultsDiv.innerHTML = resultHTML;
			introParagraph.innerHTML = tChartText;
			calcOuterValues(ttlTodalHhd, resWin, ttlFlush, ttlShower + ttlBath, ttlTooth, ttlLaundry, ttlDW, ttlClean, ttlDrink);
		} else {
			setTimeout(updateResults, 50);
		}
	}

	// setTimeout(function() {
	// 	//resWin.onload = function() {
	// 	resWin.document.getElementById("IResults").innerHTML = resultHTML;
	// 	resWin.document.getElementById("intPar").innerHTML = tChartText;
	// 	calcOuterValues(ttlTodalHhd, resWin, ttlFlush, ttlShower + ttlBath, ttlTooth, ttlLaundry, ttlDW, ttlClean, ttlDrink);
	// },100);
	//}


   // if (document.getElementById("hasOut").checked==false)
   // {
	   // calcOuterValues(ttlTodalHhd);
   // }
}

$(function(){
	$('body').on('change', 'input[type=text]', function(){
		$(this).val($(this).val().replace(/\D/g, ''));
	});
});
